var app = angular.module('myApp', []);

app.config(['$interpolateProvider', function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
}]);


app.controller('MyCtrl', function($scope){
  var socket = io.connect('http://' + document.domain + ':' + location.port );
  $scope.info = "Everything works!";
  $scope.pingpong = 0;
  $scope.responses = [];
  var ping_pong_times = [];
  var start_time;
  window.setInterval(function() {
         start_time = (new Date).getTime();
         socket.emit('my_ping');
            }, 1000);

   

  $scope.echoIt = function() {
        console.log("In echoIt "+ $scope.emit_data );
        socket.emit("my_echo_event", $scope.emit_data);
    }
    

  $scope.broadcastIt = function() {
        console.log("In broadcastIt "+ $scope.broadcast_data );
        socket.emit("my_broadcast_event", $scope.broadcast_data);
    }
 

  $scope.joinRoom = function() {
        console.log("In Join Room "+ $scope.join_room_name );
        socket.emit("join", $scope.join_room_name);
    }
 
  $scope.leaveRoom = function() {
        console.log("In Leave Room "+ $scope.leave_room_name );
        socket.emit("leave", $scope.leave_room_name);
    }
 
  $scope.sendToRoom = function() {
        console.log("In Leave Room "+ $scope.leave_room_name );
        socket.emit("send_message_to_room", {'data': $scope.send_room_message,'room': $scope.send_room_name});
    }
 
  $scope.disconnectRequest = function() {
        console.log("Disconnecting"+ $scope.leave_room_name );
        socket.emit("disconnect_request");
    }
 

   socket.on('connect', function(){
       console.log("CONNECTED");
     });


   socket.on('my_response', function(msg_received){
   	   $scope.responses.push(msg_received);
   	   $scope.$apply();
       console.log("received");
       var element = document.getElementById('responsePane');
       element.scrollTop = element.scrollHeight;
     });


   socket.on('my_pong', function() {
        var latency = (new Date).getTime() - start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
        var sum = 0;
        for (var i = 0; i < ping_pong_times.length; i++)
            sum += ping_pong_times[i];
        $scope.pingpong = Math.round(10 * sum / ping_pong_times.length) / 10;
        $scope.$apply();
    });   



});


console.log("Fixed It Sooner");
